import { Component } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  public textInput = '';
  public responseEntities = [];
  public formDisabled = false;

  constructor(
    private api: ApiService
  ) {

  }

  public async onSubmit() {
    this.formDisabled = true;
    const response = await this.api.processText(this.textInput);
    //@ts-ignore
    this.responseEntities = response.entities;
    this.formDisabled = false;
  }
}
