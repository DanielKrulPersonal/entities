import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  public processText(text: string) {
    const object = {
      id: '1',
      text,
      analyses: ['entities']
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'user_key ba0d8598e8acaa705ea7d1fd42fd6554'
      })
    };
    return this.http.post('https://cors-anywhere.herokuapp.com/https://api.geneea.com/v3/analysis', object, httpOptions).toPromise();
  }
}
